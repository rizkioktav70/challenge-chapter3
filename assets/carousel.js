$('.owl-carousel').owlCarousel({
    loop: true,
    center: true,
    nav: true,
    navText: ["<img class='button-carousel' id='left-button' src='assets/icon/Left_button_hover.png'>",
        "<img class='button-carousel' id='right-button' src='assets/icon/Right_button_hover.png'>"],
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 2000,
    responsiveClass: true,
    responsive: {
        360: {
            items: 1,
            nav: true
        },
        600: {
            items: 2
        },
    }
})


function openNav() {
    document.getElementById("mySidebar").style.width = "180px";
    document.getElementById("mySidebarDark").style.width = "360px";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("mySidebarDark").style.width = "0";
}
